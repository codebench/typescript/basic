function foo<T> (bar: T, baz: (T) => void) {
    const test: T = bar;
    baz(test);
}

const str: string = "a";
// foo<string>(str, (num: string) => num.parseInt());
// foo<string>("a", num => num.parseInt());

function x<T>(arg: T) {
  arg.parseInt();
}
foo<string>(str, x);

