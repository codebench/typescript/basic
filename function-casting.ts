/*
type T = (x: number) => boolean;

let fn = function(a: string, b: boolean, c: T){};

fn('yes', true, (()=> {

}) as T);
*/

interface ttype { (x: number): boolean }
var c : ttype = (x: number) => {
  return false;
}
var d = () => {
  return true;
}

var x = c(3);
console.log("x = " + x);

var y = d();
console.log("y = " + y);


let fn = function(a: string, b: boolean, c: ttype){};
fn("a", true, c);
fn("a", true, d as ttype);
fn("a", true, (() => { return true; }) as ttype);
